package com.itau.velha.business;

public class Tabuleiro {

	private String casas[][] = new String[3][3];
	private String[] jogadores = {"X", "O"};
	private int jogadorAtivo = 0;
	
	public Tabuleiro() {
		for (int i = 0; i < casas.length; i++) {
			for (int j = 0; j < casas.length; j++) {
				casas[i][j] = " ";
			}
		}
	}
	
	public void jogar(int x, int y) {
		String jogada = jogadores[jogadorAtivo];
		
		casas[x][y] = jogada;
		
		trocarJogador();
	}
	
	private void trocarJogador() {
		if(jogadorAtivo == 1) {
			jogadorAtivo = 0;
		}else {
			jogadorAtivo = 1;
		}
	}
	
	public String[][] getCasas(){
		return casas;
	}
	
	public int getJogadorAtivo() {
		return jogadorAtivo;
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		for (String[] linha: casas) {
			for(String casa: linha) {
				builder.append(casa + "|");
			}
			builder.append("\n");
		}
		
		return builder.toString();
	}
}
