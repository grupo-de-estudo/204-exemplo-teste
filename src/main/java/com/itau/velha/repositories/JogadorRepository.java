package com.itau.velha.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.velha.models.Jogador;

public interface JogadorRepository extends CrudRepository<Jogador, Long> {
	
}
