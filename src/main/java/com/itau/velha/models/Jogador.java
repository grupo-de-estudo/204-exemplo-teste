package com.itau.velha.models;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Jogador {

	@Id
	private long id;
	
	private String nome;
	private int pontuacao;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getPontuacao() {
		return pontuacao;
	}
	public void setPontuacao(int pontuacao) {
		this.pontuacao = pontuacao;
	}
}
