package com.itau.velha.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.velha.business.Tabuleiro;
import com.itau.velha.models.Jogador;
import com.itau.velha.repositories.JogadorRepository;

@RestController
public class JogoController {
	Tabuleiro tabuleiro = new Tabuleiro();
	
	@Autowired
	JogadorRepository jogadorRepository;
	
	@RequestMapping("/jogo")
	public Tabuleiro jogar() {
		return tabuleiro;
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/jogador")
	public Jogador salvarJogador(@RequestBody Jogador jogador) {
		return jogadorRepository.save(jogador);
	}
}
