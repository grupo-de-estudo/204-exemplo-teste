package com.itau.velha.business;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TabuleiroTest {
	
	@Test
	public void testarInicializacao() {
		Tabuleiro tabuleiro = new Tabuleiro();
		
		String string = tabuleiro.toString();
		
		System.out.println(string);
		
		assertEquals(string, " | | |\n | | |\n | | |\n");
	}
	
	@Test
	public void testarJogada() {
		Tabuleiro tabuleiro = new Tabuleiro();
		
		tabuleiro.jogar(0, 0);
		
		String string = tabuleiro.toString();
		
		System.out.println(string);
		
		assertEquals(string, "X| | |\n | | |\n | | |\n");
	}
	
	@Test
	public void testarTrocaDeJogador() {
		Tabuleiro tabuleiro = new Tabuleiro();
		
		tabuleiro.jogar(0, 0);
		tabuleiro.jogar(1, 0);
		tabuleiro.jogar(2, 0);
		
		String string = tabuleiro.toString();
		
		System.out.println(string);
		
		assertEquals(string, "X| | |\nO| | |\nX| | |\n");
	}
}
