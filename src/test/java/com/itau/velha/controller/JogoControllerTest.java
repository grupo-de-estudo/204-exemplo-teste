package com.itau.velha.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.velha.controllers.JogoController;
import com.itau.velha.models.Jogador;
import com.itau.velha.repositories.JogadorRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(JogoController.class)
public class JogoControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	JogadorRepository jogadorRepository;
	
	ObjectMapper mapper = new ObjectMapper();
	
	@Test
	public void testarJogar() throws Exception {
		mockMvc.perform(get("/jogo")).andExpect(jsonPath("$.jogadorAtivo", equalTo(0)));
	}
	
	@Test
	public void testarCriarJogador() throws Exception {
		Jogador jogador = new Jogador();
		
		jogador.setNome("José");
		jogador.setPontuacao(2);
		
		when(jogadorRepository.save(any(Jogador.class))).thenReturn(jogador);
		
		String json = mapper.writeValueAsString(jogador);
		
		mockMvc.perform(post("/jogador")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.nome", equalTo("José")));
	}
}
